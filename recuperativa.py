#!/usr/bin/python
# -*- coding: utf-8 -*-


import json

# Carga la informacion desde el archivo "iris.json"


def load_file():

    with open("iris.json", 'r') as file:
        iris = json.load(file)
        # Muestra en pantalla el archivo cargado
    for i in iris:
        print("{0}".format(i))
    return iris

# Genera archivos .json con las especies,
# las variables se encargan de separar por especie


def save_file(diccionario, nombre):

    with open(nombre + ".json", 'w') as file:
        json.dump(diccionario, file)

# Genera los datos a ser guardados en json,
# genera las variables diccionario y especies


def guardar_en_json():
    diccionario = []
    for i in range(len(especies)):
        for j in iris:
            if j['species'] == especies[i]:
                diccionario.append(j)
        print(json.dumps(diccionario))
        save_file(diccionario, especies[i])
        diccionario = []

# define por especie una sumatoria y posterior divsion,
# para así obtener el promedio de altura por especie


def calcula_promedio_altura():
    promedio_altura = [0, 0, 0]
    for i in iris:
        for j in range(len(especies)):
            if i['species'] == especies[j]:
                promedio_altura[j] += i['petalLength']
    for i in range(len(especies)):
        promedio_altura[i] = round(promedio_altura[i]/50, 1)
    return promedio_altura

# define por especie una sumatoria y posterior divsion,
# para así obtener el promedio de anchura por especie


def calcula_promedio_anchura():
    promedio_anchura = [0, 0, 0]
    for i in iris:
        for j in range(len(especies)):
            if i['species'] == especies[j]:
                promedio_anchura[j] += i['petalWidth']
    for i in range(len(especies)):
        promedio_anchura[i] = round(promedio_anchura[i]/50, 1)
    return promedio_anchura


# se vale de los valores de anchura
# y altura obtenidos anteriormente para
# identificar la especia con petalo mas alto y ancho
def especie_mas_alto_ancho():
    promedios_especies = {especies[i]: [promedio_anchura[i],
                                        promedio_altura[i]]
                          for i in range(len(especies))}
    texto = 'La especie con el petalo más alto y ancho es {0} ' \
            'con {1} de alto y {2} de ancho'
    max_altura = max(promedio_altura)
    max_anchura = max(promedio_anchura)
    for key, value in promedios_especies.items():
        if max_altura in value and max_anchura in value:
            print(texto.format(key, max_altura, max_anchura))

# Genera una lista con todos los valores de los cépalos de todas la flores,
# luego ingresa a una variable el mas grande,
# este se comparará con todos y extrae el que sea igual al mayor


def medida_maxima_sepalo():
    largo_sepalos = []
    texto = 'La especie con la sépa mas alta es {0} con un largo de {1}'
    for i in iris:
        largo_sepalos.append(i["sepalLength"])
    largo_maximo = max(largo_sepalos)
    for i in iris:
        if largo_maximo in i.values():
            print(texto.format(i['species'], i["sepalLength"]))

# establece las flores que entran en un rango respecto del promedio,
# esto según la especie, luego imprime las coincidencias


def rango_promedios():
    rango_w = [0, 0, 0]
    rango_l = [0, 0, 0]

    for i in iris:
        for j in range(len(especies)):
            inferior_w = (promedio_anchura[0] - 3)
            superior_w = (promedio_anchura[0] + 3)
            if inferior_w < i['petalWidth'] \
                    < superior_w and i['species'] == especies[0]:
                rango_w[j] += 1
    for i in iris:
        for j in range(len(especies)):
            inferior_l = (promedio_anchura[0] - 3)
            superior_l = (promedio_anchura[0] + 3)
            if inferior_l < i['petalLength'] \
                    < superior_l and i['species'] == especies[0]:
                rango_l[j] += 1
    texto = 'Dentro del rango +-3 con'\
            'respecto al promedio:\nEspecie {0} tiene {1} dentro del rango ' \
            'en la altura y {2} dentro del rango en la anchura '
    for i in range(len(especies)):
        print(texto.format(especies[i], rango_l[i], rango_w[i]))

# extrae las especies del total del archivo,
# esto lo pasa a una tupla,
# esta tupla de especies será utilizada como parametro fijo
# dentro del programa


def species():
    tempespecies = []
    especies = []
    for i in iris:
        for keys, value in i.items():
            if keys == 'species':
                tempespecies.append(value)
    for i in tempespecies:
        if i not in especies:
            especies.append(i)
    return especies


# main llama a las funciones que buscan responder a la evaluación
if __name__ == '__main__':
    print("Se cargar los archivos de iris.json:")
    print("*******************************************************")
    iris = load_file()
    especies = species()
    print("las especies son:")
    print("*******************************************************")
    for i in especies:
        print("Especie:", i)
    print("\nLos promedios de altura y anchura pétalos son: ")
    print("*******************************************************")
    promedio_altura = calcula_promedio_altura()
    promedio_anchura = calcula_promedio_anchura()
    texto = 'especie: {0} promedio altura: {1} promedio anchura {2}'
    for i in range(len(especies)):
        print(texto.format(especies[i], promedio_altura[i],
              promedio_anchura[i]))
    print("\nLa especie con el pétalo más alto y ancho es: ")
    print("*******************************************************")
    especie_mas_alto_ancho()
    print("\nLa cantidad que entra dentro del rango es por especie de: ")
    print("*******************************************************")
    rango_promedios()
    print("\nLa especie con la sépa mas alta es: ")
    print("*******************************************************")
    medida_maxima_sepalo()
    print("\nSe guarda en archivos json por especie: ")
    print("*******************************************************")
    guardar_en_json()
    
